(require "asdf")

;; Store FASLs in the autopkgtest temporary directory.
(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP")))
  (asdf:load-system "osicat/tests"))

;; We can't use ASDF:TEST-SYSTEM because its return value does not indicate
;; whether any tests failed.
(unless (osicat/tests:run) (uiop:quit 1))
